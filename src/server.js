import express from 'express';
import configViewEngine from './configs/viewEngine';
import initWebRoute from './route/web';
import initAPIRoute from './route/api'
var bodyParser = require('body-parser')
// import connection from './configs/connectDB';
require('dotenv').config();
const app = express();
const port = process.env.PORT || 8080;
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// setup view engine
configViewEngine(app);

// init route
initWebRoute(app);
initAPIRoute(app)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
}) 